# `rglslsp`

`rglslsp` is a program that implements a Language Server for the OpenGL Shader
Language.

It is written in Rust as both a way to practice and a way to integrate more
closely in a few other tools I use.

It could probably be more efficient, use higher level libraries, and be more
correct. Buyer beware.
