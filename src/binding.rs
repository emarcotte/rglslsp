use lsp_types::{
    notification::{
        Notification,
    },
    request::{
        Request as Method,
    }
};

use std::{
    error::{
        Error,
    },
    marker::{
        PhantomData,
    },
};

use serde::{
    Serialize,
    de::{
        DeserializeOwned,
    },
};

// TODO: Can these traits be made to have associated types rather than generics?
pub trait NotificationHandler<Rq>
where
    Rq: Notification,
    Rq::Params: DeserializeOwned,
{
    fn method(&self) -> &'static str { Rq::METHOD }

    fn process(&self, params: &Rq::Params) -> Result<(), Box<dyn Error>>;
}

// TODO: Can these traits be made to have associated types rather than generics?
pub trait MethodHandler<Rq>
where
    Rq: Method,
    Rq::Result: Serialize,
    Rq::Params: DeserializeOwned,
{
    fn method(&self) -> &'static str { Rq::METHOD }

    fn process(&self, params: &Rq::Params) -> Result<Rq::Result, Box<dyn Error>>;
}

pub struct RequestMethod<Rq> where Rq: Method {
    pub marker: PhantomData<Rq>,
}

pub struct RequestNotification<Rq> where Rq: Notification {
    pub marker: PhantomData<Rq>,
}

/*
#[cfg(test)]
mod test {
    use super::*;
    use serde_json::json;

    macro_rules! err_result {
        ($x:expr, $msg:expr) => {
            let result = $x;
            if result.is_err() {
                let message = result
                    .unwrap_err()
                    .to_string();

                assert_eq!(
                    message,
                    $msg
                );
            }
            else {
                assert!(false, format!("Expected error: {}", $msg));
            }
        }
    }

    macro_rules! ok_result {
        ($x:expr) => {
            {
                let is_ok = $x.is_ok();
                if is_ok {
                    assert!(true, "OK Result");
                }
                else {
                    assert!(false, $x.unwrap_err().to_string());
                }
            }
        }
    }

    #[test]
    fn handle() {
        ok_result!(MethodBinding::Initialize(Box::new(move |_, _| { }))
            .handle(&json!({
                "id": "1234,",
                "method": "initialize",
                "params": {
                    "capabilities": {},
                }})));

        ok_result!(MethodBinding::Initialized(Box::new(move |_, _| { }))
            .handle(&json!({
                "method": "garbage",
                "params": {}})));

        ok_result!(MethodBinding::TextDocumentDidOpen(Box::new(move |_, _| { }))
            .handle(&json!({
                "method": "garbage",
                "params": {}})));

        err_result!(
            MethodBinding::Initialize(Box::new(move |_, _| {}))
                .handle(&json!({ "method": [], "params": {}})),
            "'method' attribute is not a string"
        );

        err_result!(
            MethodBinding::Initialize(Box::new(move |_, _| {}))
                .handle(&json!({ "params": {}})),
            "'method' attribute missing"
        );
    }
}
*/
