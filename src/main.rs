mod codec;
mod binding;
mod method;

use glsl::{
    parser::{
        Parse,
    },
    syntax::{
        ShaderStage,
    },
};

use futures::{
    StreamExt,
};

use serde_json::{
    Value,
    json,
    to_value,
};

use serde::{
    Serialize,
    de::{
        DeserializeOwned,
    },
};

use std::{
    collections::{
        HashMap,
    },
    error::{
        Error,
    },
    io::{
        Stdout,
        Write,
    },
};

use crate::{
    binding::{
        MethodHandler,
        NotificationHandler,
        RequestNotification,
    },
    codec::{
        JSONRPCCodec,
        MethodRequest,
        NotificationRequest,
        Payload,
        Request as RPC,
    },
    method::{
        InitializeHandler,
    },
};

use log::{
    debug,
    error,
    warn,
};

use lsp_types::{
    DidChangeTextDocumentParams,
    notification::{
        Notification,
        DidChangeTextDocument,
    },
    request::{
        Request as Method,
    },
};

use tokio_util::{
    codec::{
        FramedRead,
    },
};

use tokio::{
    io::{
        stdin,
        AsyncRead,
    },
};

fn run_diag() {
    let glsl = "(1vec3(r, g, b) * cos(t * PI * .5)).xxz";
    let expr = ShaderStage::parse(glsl);
    assert!(expr.is_ok());
}

/// Handles something like type erasure for notifications (doing the conversion from Value to
/// Notification::PARAM)
type NotificationCallback = dyn Fn(&NotificationRequest) -> Result<(), Box<dyn Error>>;

/// Handles something like type erasure for methods (doing the conversion from Value to
/// Method::PARAM)
type MethodCallback = dyn Fn(&MethodRequest) -> Result<Value, Box<dyn Error>>;

// TODO: Should these be run "in the background"?
// TODO: Would be great to convert the params once and re-use across notification handlers
fn make_notification<NH, Req>(handler: NH) -> Box<NotificationCallback>
    where NH: NotificationHandler<Req>,
          NH: 'static,
          Req: Notification,
          <Req as Notification>::Params: DeserializeOwned,
{
    Box::new(move |rpc: &NotificationRequest| {
        match rpc.params::<Req::Params>()? {
            Some(params) => handler.process(&params),
            None         => Err("Parameters are required for notifications".into()),
        }
    })
}

// TODO: Make async support for methods.
fn make_method<RH, Req>(handler: RH) -> Box<MethodCallback>
    where RH: MethodHandler<Req>,
          RH: 'static,
          Req: Method,
          <Req as Method>::Result: Serialize,
          <Req as Method>::Params: DeserializeOwned,
{
    Box::new(move |rpc: &MethodRequest| {
        let result = match rpc.params::<Req::Params>()? {
            Some(params) => handler.process(&params),
            None         => Err("Parameters are required for notifications".into()),
        }?;
        Ok(to_value(result).or_else(|e| Err(Box::new(e)))?)
    })
}

struct App<InputType>
{
    methods:       HashMap<&'static str, Box<MethodCallback>>,
    notifications: HashMap<&'static str, Box<NotificationCallback>>,
    reader:        FramedRead<InputType, JSONRPCCodec>,
    stdout:        Stdout,
}

impl<InputType> App<InputType>
where
    InputType: AsyncRead + Unpin,
{
    fn new(input: InputType) -> App<InputType> {
        App {
            notifications: HashMap::new(),
            methods: HashMap::new(),
            stdout: std::io::stdout(),
            reader: FramedRead::new(
                input,
                JSONRPCCodec::new()
            ),
        }
    }

    pub fn add_notification<NH, Req>(&mut self, handler: NH)
        where NH: NotificationHandler<Req>,
              NH: 'static,
              Req: Notification,
              <Req as Notification>::Params: DeserializeOwned,
    {
        let method = handler.method();
        self.notifications.insert(method, make_notification(handler));
    }

    pub fn add_method<RH, Req>(&mut self, handler: RH)
        where RH: MethodHandler<Req>,
              RH: 'static,
              Req: Method,
              <Req as Method>::Result: Serialize,
              <Req as Method>::Params: DeserializeOwned,
    {
        let method = handler.method();
        self.methods.insert(method, make_method(handler));
    }

    fn send_result(&mut self, id: &codec::ID, result: &Value) -> Result<(), Box<dyn Error>> {
        let request = json!({
            "jsonrpc": "2.0",
            "result": result,
            "id": id,
        });
        let serialized_string = request.to_string();
        let serialized_bytes = serialized_string.as_bytes();

        // TODO: Convert to async.
        let mut handle = self.stdout.lock();
        handle.write_all(format!("Content-Length: {}\r\n\r\n", serialized_bytes.len()).as_bytes())?;
        handle.write_all(serialized_bytes)?;
        Ok(())
    }

    fn handle(&mut self, frame: &Payload) -> Result<(), Box<dyn Error>> {
        match frame {
            Payload::Single(request) => {
                match request {
                    RPC::Invalid => {
                        error!("Invalid request processing here");
                        Ok(())
                    },
                    RPC::Method(rpc) => {
                        self.run_method(rpc)
                    },
                    RPC::Notification(rpc) => {
                        self.run_notification(rpc)
                    },
                }
            },
            Payload::Batch(_requests) => {
                error!("TODO: Implement batches");
                Ok(())
            },
            Payload::Invalid => {
                error!("Invalid payload");
                Ok(())
            },
        }
    }

    fn run_notification(&mut self, rpc: &codec::NotificationRequest) -> Result<(), Box<dyn Error>> {
        match self.notifications.get(rpc.method()) {
            Some(handler) => {
                handler(rpc)
            },
            None => {
                debug!("No bindings for {}", rpc.method());
                Ok(())
            },
        }
    }

    fn run_method(&mut self, rpc: &codec::MethodRequest) -> Result<(), Box<dyn Error>> {
        match self.methods.get(rpc.method()) {
            Some(handler) => {
                // TODO: Send error responses...
                let result = handler(rpc)?;
                self.send_result(&rpc.id(), &result)
            },
            None => {
                debug!("No bindings for {}", rpc.method());
                Ok(())
            },
        }
    }

    pub async fn run(&mut self) {
        while let Some(frame) = self.reader.next().await {
            match frame {
                Ok(frame) => {
                    if let Err(e) = self.handle(&frame) {
                        warn!("binding err {:?}", e);
                    }
                },
                Err(e) => error!("Codec read error {:?}", e),
            };
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    env_logger::builder()
        .filter_level(log::LevelFilter::Debug)
        .init();

    let mut app = App::new(stdin());

    app.add_method(InitializeHandler::new());
    app.add_notification(DidChangeTextDocumentHandler::new());

    app.run().await;

    Ok(())
}

pub type DidChangeTextDocumentHandler = RequestNotification<DidChangeTextDocument>;

impl DidChangeTextDocumentHandler {
    pub fn new() -> DidChangeTextDocumentHandler {
        DidChangeTextDocumentHandler {
            marker: std::marker::PhantomData,
        }
    }
}

impl NotificationHandler<DidChangeTextDocument> for DidChangeTextDocumentHandler {
    fn process(&self, _params: &DidChangeTextDocumentParams) -> Result<(), Box<dyn Error>> {
        debug!("Text document change...");
        Ok(())
    }
}

