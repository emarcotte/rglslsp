use tokio::{
    io,
};

use tokio_util::{
    codec::{
        Decoder,
    },
};

use std::{
    collections::{
        HashMap,
    },
    error::{
        Error,
    },
};

use bytes::{
    Buf,
    BytesMut,
};

use serde::{
    Deserialize,
    Serialize,
};

use serde_json::{
    Value,
    from_value,
};

#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct MethodRequest {
    params: Option<Value>,
    method: String,
    id: ID,
}

impl MethodRequest {
    pub fn params<T>(&self) -> Result<Option<T>, Box<dyn Error>> 
    where
        T: for<'de> Deserialize<'de>
    {
        if let Some(params) = &self.params {
            let params = from_value::<T>(params.clone())?;
            Ok(Some(params))
        }
        else {
            Ok(None)
        }
    }

    pub fn method(&self) -> &str {
        &self.method
    }

    pub fn id(&self) -> &ID {
        &self.id
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq)]
pub struct NotificationRequest {
    pub params: Option<Value>,
    pub method: String,
}

impl NotificationRequest {
    pub fn params<T>(&self) -> Result<Option<T>, Box<dyn Error>> 
    where
        T: for<'de> Deserialize<'de>
    {
        if let Some(params) = &self.params {
            let params = from_value::<T>(params.clone())?;
            Ok(Some(params))
        }
        else {
            Ok(None)
        }
    }

    pub fn method(&self) -> &str {
        &self.method
    }
}

#[derive(Deserialize, Serialize, Debug, PartialEq)]
#[serde(untagged)]
pub enum Request {
    Method(MethodRequest),
    Notification(NotificationRequest),
    Invalid,
}

#[derive(Deserialize, Serialize, Debug, PartialEq)]
#[serde(untagged)]
pub enum ID {
    String(String),
    Number(u64),
    Null,
}

#[derive(Deserialize, Serialize, Debug, PartialEq)]
#[serde(untagged)]
pub enum Payload {
    Batch(Vec<Request>),
    Single(Request),
    /// TODO: Should generate: {"jsonrpc": "2.0", "error": {"code": -32600, "message": "Invalid Request"}, "id": null},
    Invalid,
}

enum DecodeState {
    InHeader,
    InBody(usize),
}

/*
fn get_id(frame: &Value) -> Result<Option<ID>, &'static str> {
    let id = frame.get("id");
    match id {
        None                   => Ok(None),
        Some(Value::Null)      => Ok(Some(Value::Null)),
        Some(Value::Number(a)) => Ok(Some(Value::Number(a.clone()))),
        Some(Value::String(s)) => Ok(Some(Value::String(s.clone()))),
        _ => Err("Invalid ID")
    }
}

impl From<&Value> for Request {
    fn from(frame: &Value) -> Self {
        let method = match frame.get("method") {
            Some(v) => v,
            None    => return Request::Invalid,
        };

        let method = match method.as_str() {
            Some(s) => s.to_owned(),
            None    => return Request::Invalid,
        };

        // Params may be an object, array or missing.
        let params = frame.get("params")
            .cloned();

        match get_id(&frame) {
            Ok(Some(id)) => Request::MethodRequest(MethodRequest {
                params,
                method,
                id,
            }),
            Ok(None)     => Request::NotificationRequest(NotificationRequest {
                params,
                method,
            }),
            Err(_)       => Request::Invalid
        }
    }
}

impl From<&Value> for Payload {
    fn from(value: &Value) -> Self {
        match value {
            Value::Array(values) => {
                let requests = values.iter()
                    .map(Request::from)
                    .collect::<Vec<_>>();

                if requests.is_empty() {
                    Payload::Invalid
                }
                else {
                    Payload::Batch(requests)
                }
            },
            Value::Object(_) => {
                Payload::Single(Request::from(value))
            },
            _ => {
                Payload::Invalid
            }
        }
    }
}
*/

pub struct JSONRPCCodec {
    current_headers: HashMap<String, String>,
    state: DecodeState,
}

impl JSONRPCCodec {

    pub fn new() -> JSONRPCCodec {
        JSONRPCCodec {
            state: DecodeState::InHeader,
            current_headers: HashMap::new(),
        }
    }

    fn decode_header_line(&mut self, buf: &mut BytesMut) -> Result<Option<()>, String> {
        if let Some(newline_offset) = buf.iter().position(|b| *b == b'\n') {
            let line = String::from_utf8_lossy(&buf[0 .. newline_offset - 1]);
            if line.is_empty() {
                let len: usize = self.current_headers.get("Content-Length")
                    .ok_or_else(|| "Missing header Content-Length")?
                    .parse()
                    .or_else(|_| Err("Malformed Content-Length header"))?;
                self.state = DecodeState::InBody(len);
            }
            else {
                let mut bits = line.split(": ");
                let key = bits.next()
                    .ok_or_else(|| ("Malformed header"))?;
                let value = bits.next()
                    .ok_or_else(|| ("Malformed header"))?;
                self.current_headers.insert(key.to_owned(), value.to_owned());
            }
            buf.advance(newline_offset + 1);
            Ok(Some(()))
        }
        else {
            Ok(None)
        }
    }
}

impl Decoder for JSONRPCCodec {
    type Item = Payload;
    type Error = io::Error;

    fn decode(&mut self, buf: &mut BytesMut) -> Result<Option<Self::Item>, Self::Error> {
        loop {
            let len = buf.len();
            if len == 0 {
                return Ok(None);
            }

            match self.state {
                DecodeState::InHeader => {
                    match self.decode_header_line(buf) {
                        Err(e)   => Err(io::Error::new(io::ErrorKind::InvalidData, e)),
                        Ok(None) => return Ok(None),
                        _        => Ok(Some(())),
                    }?;
                },
                DecodeState::InBody(size) => {
                    if len < size {
                        return Ok(None);
                    }
                    else {
                        // Lossy parse to avoid weird bad data in messages.
                        let payload = serde_json::from_slice::<Payload>(&buf[0 .. size])?;
                        buf.advance(size);
                        self.state = DecodeState::InHeader;
                        self.current_headers.clear();
                        return Ok(Some(payload));
                    }
                },
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::{
        io::{
            Result,
        },
    };
    use serde_json::{
        json,
    };

    fn consume(codec: &mut JSONRPCCodec, bytes: &mut BytesMut) -> Result<Payload> {
        loop {
            if let Some(output) = codec.decode(bytes)? {
                return Ok(output);
            }
        }
    }

    fn generate_msg(msg: &str) -> String {
        let len = msg.len();
        format!("Content-Length: {}\r\n\r\n{}", len, msg)
    }

    fn consume_with_header(decoder: &mut JSONRPCCodec, msg: &str) -> Result<Payload> {
        let msg = generate_msg(msg);
        consume(decoder, &mut BytesMut::from(msg.as_bytes()))
    }

    #[test]
    fn test_decode() {
        let mut decoder = JSONRPCCodec::new();
        assert_eq!(
            consume_with_header(&mut decoder, "{\"method\":\"helloWorld\"}")
                .unwrap(),
            Payload::Single(
                Request::Notification(NotificationRequest {
                    method: "helloWorld".into(),
                    params: None,
                }),
            ),
            "Can parse simple notifications",
        );

        assert_eq!(
            consume_with_header(&mut decoder, "{\"method\":\"helloWorld\", \"id\": \"id_here\"}")
                .unwrap(),
            Payload::Single(
                Request::Method(MethodRequest {
                    id: ID::String("id_here".into()),
                    method: "helloWorld".into(),
                    params: None,
                }),
            ),
            "Can parse simple notifications",
        );

        assert_eq!(
            consume_with_header(&mut decoder, "{\"method\":\"helloWorld\", \"id\": \"id_here\", \"params\":{\"a\":1}}")
                .unwrap(),
            Payload::Single(
                Request::Method(MethodRequest {
                    id: ID::String("id_here".into()),
                    method: "helloWorld".into(),
                    params: Some(json!({
                        "a": 1,
                    })),
                }),
            ),
            "Can parse simple notifications",
        );

        // TODO: Make sure these are failing the right way?
        assert!(
            consume(&mut decoder, &mut BytesMut::from("Content-Length: a6\r\n\r\n{\"test\":\"value\"}"))
                .is_err(),
            "Invalid header should return errors",
        );

        assert!(
            consume_with_header(&mut decoder, "{\"test")
                .is_err(),
            "Invalid json should return errors",
        );

        assert!(
            consume_with_header(&mut decoder, "{}")
                .is_err(),
            "Invalid json rpc payload should return errors",
        );
    }

    #[test]
    fn json_rpc_doc_examples() {
        /*
        let mut decoder = JSONRPCCodec::new();

        let result = consume_with_header(&mut decoder, r#"{"jsonrpc": "2.0", "method": "subtract", "params": [42, 23], "id": 1}"#);
        let expected = json!({"jsonrpc": "2.0", "result": 19, "id": 1});
        assert_eq!(result.unwrap(), expected, "rpc call with positional parameters");

        --> {"jsonrpc": "2.0", "method": "subtract", "params": [23, 42], "id": 2}
        <-- {"jsonrpc": "2.0", "result": -19, "id": 2}

        // rpc call with named parameters:
        --> {"jsonrpc": "2.0", "method": "subtract", "params": {"subtrahend": 23, "minuend": 42}, "id": 3}
        <-- {"jsonrpc": "2.0", "result": 19, "id": 3}

        --> {"jsonrpc": "2.0", "method": "subtract", "params": {"minuend": 42, "subtrahend": 23}, "id": 4}
        <-- {"jsonrpc": "2.0", "result": 19, "id": 4}

        // a Notification:
        --> {"jsonrpc": "2.0", "method": "update", "params": [1,2,3,4,5]}
        --> {"jsonrpc": "2.0", "method": "foobar"}

        // rpc call of non-existent method:
        --> {"jsonrpc": "2.0", "method": "foobar", "id": "1"}
        <-- {"jsonrpc": "2.0", "error": {"code": -32601, "message": "Method not found"}, "id": "1"}

        // rpc call with invalid JSON:
        --> {"jsonrpc": "2.0", "method": "foobar, "params": "bar", "baz]
        <-- {"jsonrpc": "2.0", "error": {"code": -32700, "message": "Parse error"}, "id": null}

        // rpc call with invalid Request object:
        --> {"jsonrpc": "2.0", "method": 1, "params": "bar"}
        <-- {"jsonrpc": "2.0", "error": {"code": -32600, "message": "Invalid Request"}, "id": null}

        // rpc call Batch, invalid JSON:
        --> [
            {"jsonrpc": "2.0", "method": "sum", "params": [1,2,4], "id": "1"},
            {"jsonrpc": "2.0", "method"
        ]
        <-- {"jsonrpc": "2.0", "error": {"code": -32700, "message": "Parse error"}, "id": null}

        // rpc call with an empty Array:
        --> []
        <-- {"jsonrpc": "2.0", "error": {"code": -32600, "message": "Invalid Request"}, "id": null}

        // rpc call with an invalid Batch (but not empty):
        --> [1]
        <-- [
            {"jsonrpc": "2.0", "error": {"code": -32600, "message": "Invalid Request"}, "id": null}
        ]

        // rpc call with invalid Batch:
        --> [1,2,3]
        <-- [
            {"jsonrpc": "2.0", "error": {"code": -32600, "message": "Invalid Request"}, "id": null},
            {"jsonrpc": "2.0", "error": {"code": -32600, "message": "Invalid Request"}, "id": null},
            {"jsonrpc": "2.0", "error": {"code": -32600, "message": "Invalid Request"}, "id": null}
        ]

        // rpc call Batch:
        --> [
            {"jsonrpc": "2.0", "method": "sum", "params": [1,2,4], "id": "1"},
            {"jsonrpc": "2.0", "method": "notify_hello", "params": [7]},
            {"jsonrpc": "2.0", "method": "subtract", "params": [42,23], "id": "2"},
            {"foo": "boo"},
            {"jsonrpc": "2.0", "method": "foo.get", "params": {"name": "myself"}, "id": "5"},
            {"jsonrpc": "2.0", "method": "get_data", "id": "9"} 
        ]
        <-- [
            {"jsonrpc": "2.0", "result": 7, "id": "1"},
            {"jsonrpc": "2.0", "result": 19, "id": "2"},
            {"jsonrpc": "2.0", "error": {"code": -32600, "message": "Invalid Request"}, "id": null},
            {"jsonrpc": "2.0", "error": {"code": -32601, "message": "Method not found"}, "id": "5"},
            {"jsonrpc": "2.0", "result": ["hello", 5], "id": "9"}
        ]

        // rpc call Batch (all notifications):
        --> [
            {"jsonrpc": "2.0", "method": "notify_sum", "params": [1,2,4]},
            {"jsonrpc": "2.0", "method": "notify_hello", "params": [7]}
        ]
        <-- //Nothing is returned for all notification batches
        */
    }
}
