use crate::{
    binding::{
        MethodHandler,
        RequestMethod,
    },
};

use lsp_types::{
    InitializeParams,
    InitializeResult,
    ServerCapabilities,
    ServerInfo,
    request::{
        Initialize,
    },
};

use std::{
    error::{
        Error,
    },
};

pub type InitializeHandler = RequestMethod<Initialize>;

impl InitializeHandler {
    pub fn new() -> InitializeHandler {
        InitializeHandler {
            marker: std::marker::PhantomData,
        }
    }
}

impl MethodHandler<Initialize> for InitializeHandler {
    fn process(&self, _params: &InitializeParams) -> Result<InitializeResult, Box<dyn Error>> {
        Ok(InitializeResult {
            capabilities: ServerCapabilities {
                code_action_provider: None,
                code_lens_provider: None,
                color_provider: None,
                completion_provider: None,
                declaration_provider: None,
                definition_provider: None,
                document_formatting_provider: None,
                document_highlight_provider: None,
                document_link_provider: None,
                document_on_type_formatting_provider: None,
                document_range_formatting_provider: None,
                document_symbol_provider: None,
                execute_command_provider: None,
                folding_range_provider: None,
                hover_provider: None,
                implementation_provider: None,
                references_provider: None,
                rename_provider: None,
                signature_help_provider: None,
                text_document_sync: None,
                type_definition_provider: None,
                workspace: None,
                workspace_symbol_provider: None,
            },
            server_info: Some(ServerInfo {
                name: "rglslsp".into(),
                version: Some("0.0.1".into()),
            }),
        })
    }
}
