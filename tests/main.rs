use std::{
    path::{
        PathBuf,
    },
    io::{
        Result,
        Error,
        ErrorKind,
        Write,
    },
    process::{
        Child,
        ChildStdin,
        Command,
        Output,
        Stdio,
    },
};

use url::{
    Url,
};

use lsp_types::{
    *
};

use serde_json::json;
use serde::{
    Serialize,
};

fn get_bin() -> Result<PathBuf> {
    std::env::current_exe()?
        .parent()
        .and_then(|parent| parent.parent())
        .and_then(|parent| Some(parent.join("rglslsp")))
        .ok_or_else(|| Error::new(ErrorKind::NotFound, "unable find directory for rglslsp".to_owned()))
        .and_then(|bin| match bin.exists() {
            false => Err(Error::new(ErrorKind::NotFound, format!("`{}` is missing", bin.to_str().unwrap()))),
            true  => Ok(bin),
        })
}

fn build_command() -> Result<Command> {
    let mut command = Command::new(get_bin()?);
    command
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .stdin(Stdio::piped());
    Ok(command)
}

fn test_run(f: &dyn (Fn(&mut Child))) -> Output {
    let mut command = build_command().unwrap();
    let mut child = command.spawn().unwrap();

    f(&mut child);

    let output = child.wait_with_output().unwrap();
    let output_string = String::from_utf8(output.stdout.clone()).unwrap();
    let err_string = String::from_utf8(output.stderr.clone()).unwrap();

    println!("sub proc stderr\n{}", err_string);
    println!("sub proc stdout\n{}", output_string);

    output
}

fn send_notification<T: Serialize>(stdin: &mut ChildStdin, method: &str, params: T) {
    let request = json!({
        "method": method,
        "params": params,
    });

    let serialized_string = request.to_string();
    let serialized_bytes = serialized_string.as_bytes();
    stdin.write(format!("Content-Length: {}\r\n\r\n", serialized_bytes.len()).as_bytes())
        .expect("Failed to write to child");
    stdin.write(serialized_bytes)
        .expect("Failed to write to child");
}

fn send_method<T: Serialize>(stdin: &mut ChildStdin, id: u32, method: &str, params: T) {
    let request = json!({
        "method": method,
        "params": params,
        "id": id,
    });
    let serialized_string = request.to_string();
    let serialized_bytes = serialized_string.as_bytes();
    stdin.write(format!("Content-Length: {}\r\n\r\n", serialized_bytes.len()).as_bytes())
        .expect("Failed to write to child");
    stdin.write(serialized_bytes)
        .expect("Failed to write to child");
}

#[test]
fn test_basic_launch() {
    let output = test_run(&|child: &mut Child| {
        let mut stdin = child.stdin
            .as_mut()
            .unwrap();

        send_method(&mut stdin, 1, "initialize", InitializeParams {
            capabilities: ClientCapabilities {
                experimental: None,
                text_document: None,
                window: None,
                workspace: None,
            },
            client_info: None,
            initialization_options: None,
            process_id: None,
            root_path: None,
            root_uri: Some(Url::parse("file:///test/path").unwrap()),
            trace: None,
            workspace_folders: None,
        });


        send_notification(&mut stdin, "initialized", InitializedParams {});
    });
/*
    assert_eq!(
        String::from_utf8(output.stdout).unwrap(),
        "{\"test\":\"value\"}\n{\"test\":\"value12\"}\n"
    );
*/
}
